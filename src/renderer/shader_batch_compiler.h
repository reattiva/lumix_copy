#pragma once

#include "shader_compiler_solo.h"
#include "engine/resource_manager_base.h"

namespace Lumix
{

struct IAllocator;
class Engine;
class FileSystemWatcher;

class ShaderBatchCompiler : public ShaderCompilerSolo
{
public:
	typedef Delegate<void(bool compiling)> CallbackDelegate;

public:
	ShaderBatchCompiler(Engine& engine, 
						const char* base_path, 
						IAllocator& allocator);
	~ShaderBatchCompiler();

	bool getDebug() const { return m_debug; }
	void setDebug(bool debug) { m_debug = debug; }
	const Array<string>& getSHDFiles() const { return m_shd_files; }
	CallbackDelegate& getCallback() { return m_callback; }

	void makeUpToDate(bool wait);
	void update();

private:
	void queueFile(const char* shd_path);
	void compileTask();
	void findShaderFiles(const char * src_dir);
	bool getSourceFromBinaryBasename(char* out, int out_max_size, const char* binary_basename);
	void parseDependencies();
	void addDependency(const char * ckey, const char * cvalue);
	void onFileChanged(const char* path);
	void processChangedFiles();
	void wait();
	void reloadShaders();

private:
	struct ShaderLoadHook : ResourceManagerBase::LoadHook
	{
		ShaderLoadHook(ResourceManagerBase& manager, ShaderBatchCompiler& compiler);
		bool onBeforeLoad(Resource& resource) override;
		ShaderBatchCompiler& m_compiler;
	};

private:
	MT::SpinMutex m_job_mutex;

	volatile int m_job_runnig;
	volatile int m_job_empty_queue;
	volatile bool m_job_exit_request;
	Array<StaticString<MAX_PATH_LENGTH>> m_job_compile_queue;
	StaticString<MAX_PATH_LENGTH> m_job_current_file;

	bool m_debug;
	CallbackDelegate m_callback;
	ShaderLoadHook m_load_hook;
	FileSystemWatcher* m_watcher;

	Array<string> m_changed_files;
	AssociativeArray<string, Array<string>> m_dependencies;
	Array<string> m_shd_files;
	Array<Resource*> m_hooked_files;
	Array<string> m_to_reload;
};

}
