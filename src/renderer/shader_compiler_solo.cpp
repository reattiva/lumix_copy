
#include "shader_compiler_solo.h"
#include "engine/engine.h"
#include "engine/log.h"
#include "engine/resource_manager.h"
#include "engine/path.h"
#include "engine/path_utils.h"
#include "engine/plugin_manager.h"
#include "engine/iplugin.h"
#include "engine/path_utils.h"
#include "engine/fs/disk_file_device.h"
#include "engine/fs/file_system.h"
#include "renderer/renderer.h"
#include "editor/platform_interface.h"
#include <cstdio>

namespace bgfx  
{
	int compileShader(int _argc, const char* _argv[]);
	typedef void(*UserErrorFn)(void*, const char*, va_list);
	void setShaderCErrorFunction(UserErrorFn fn, void* user_ptr);
}

namespace Lumix
{

ShaderCompilerSolo::ShaderCompilerSolo(Engine& engine, const char* base_path, IAllocator& allocator)
	: m_engine(engine)
	, m_allocator(allocator)
	, m_flags(0)
{
	m_is_opengl = bgfx::getRendererType() == bgfx::RendererType::OpenGL || bgfx::getRendererType() == bgfx::RendererType::OpenGLES;
	setBasePath(base_path);
}

/// Set the base path. The compiled shaders will be created in: <base path>/pipelines/compiled|compiled_gl.
/// If the base_path is an empty string, the base path of the engine filesystem will be used.
/// If it doesn't exist the output folder is created.
bool ShaderCompilerSolo::setBasePath(const char* base_path)
{
	if (!base_path)
	{
		return false;
	}

	if (stringLength(base_path) == 0)
	{
		FS::FileSystem& fs = m_engine.getFileSystem();
		FS::DiskFileDevice* disk_device = static_cast<FS::DiskFileDevice*>(fs.getDiskDevice().m_devices[0]);
		if (!disk_device)
		{
			return false;
		}
		base_path = disk_device->getBasePath();
	}

	m_base_dir << base_path;
	m_pipeline_dir << base_path << "/pipelines/";
	m_compiled_dir << base_path << "/pipelines/compiled" << (m_is_opengl ? "_gl/" : "/");

	PathUtils::normalize(m_base_dir.data);
	PathUtils::normalize(m_pipeline_dir.data);
	PathUtils::normalize(m_compiled_dir.data);

	if (!PlatformInterface::dirExists(m_compiled_dir) && !PlatformInterface::makePath(m_compiled_dir))
	{
		m_flags |= ERROR_NO_OUTPUT_FOLDER;
		g_log_error.log("ShaderCompiler") << "Compiled folder could not be created: " << m_compiled_dir;
		return false;
	}

	if (!PlatformInterface::dirExists(m_pipeline_dir))
	{
		m_flags |= ERROR_NO_PIPELINE_FOLDER;
		g_log_error.log("ShaderCompiler") << "Pipeline folder does not exist: " << m_pipeline_dir;
	}

	return true;
}

Renderer& ShaderCompilerSolo::getRenderer() const
{
	IPlugin* plugin = m_engine.getPluginManager().getPlugin("renderer");
	ASSERT(plugin);
	return static_cast<Renderer&>(*plugin);
}

/// Read and execute the shader 'shd_path' (basepath relative) as a Lua script , the assignments
/// done in it are saved in 'output'
bool ShaderCompilerSolo::getCombinations(const char* shd_path, ShaderCombinations* output) const
{
	auto& fs = m_engine.getFileSystem();
	auto* file = fs.open(fs.getDiskDevice(), Path(shd_path), FS::Mode::OPEN_AND_READ);
	if (!file)
	{
		return false;
	}

	int size = (int)file->size();
	Array<char> data(m_allocator);
	data.resize(size + 1);
	file->read(&data[0], size);
	data[size] = 0;
	fs.close(*file);

	return Shader::getShaderCombinations(shd_path, getRenderer(), &data[0], output);
}

/// Gets the vertex (_vs.sc) or fragment (_fs.sc) source filename from the
/// corresponding shd filename.
static void getShaderPath(const char* shd_path, char* out, bool vertex)
{
	PathUtils::FileInfo file_info(shd_path);
	copyString(out, MAX_PATH_LENGTH, file_info.m_dir);
	catString(out, MAX_PATH_LENGTH, file_info.m_basename);
	catString(out, MAX_PATH_LENGTH, vertex ? "_vs.sc" : "_fs.sc");
}

/// Compiles the shader if its binary is older than then the sources
bool ShaderCompilerSolo::compileIfChanged(const char* shd_path, bool debug)
{
	ShaderCombinations combinations;
	if (!getCombinations(shd_path, &combinations))
	{
		return false;
	}

	if (!isChanged(shd_path, combinations))
	{
		return false;
	}

	compile(shd_path, debug, combinations);
	return true;
}

/// Returns true if binary files are older than the source files (shd, vs.sc, fs.sc)
bool ShaderCompilerSolo::isChanged(const char* shd_path) const
{
	ShaderCombinations combinations;
	if (!getCombinations(shd_path, &combinations))
	{
		return false;
	}

	return isChanged(shd_path, combinations);
}

bool ShaderCompilerSolo::isChanged(const char* shd_path,
	const ShaderCombinations& combinations) const
{
	StaticString<MAX_PATH_LENGTH> shd_full_path(m_base_dir, shd_path);

	// Find the date of the last change in the source files (.shd, vs.sc, fs.sc)
	char tmp[MAX_PATH_LENGTH];
	auto shd_last_modified = PlatformInterface::getLastModified(shd_full_path);

	getShaderPath(shd_full_path, tmp, true);
	if (!PlatformInterface::fileExists(tmp) ||
		PlatformInterface::getLastModified(tmp) > shd_last_modified)
	{
		shd_last_modified = PlatformInterface::getLastModified(tmp);
	}

	getShaderPath(shd_full_path, tmp, false);
	if (!PlatformInterface::fileExists(tmp) ||
		PlatformInterface::getLastModified(tmp) > shd_last_modified)
	{
		shd_last_modified = PlatformInterface::getLastModified(tmp);
	}

	char basename[MAX_PATH_LENGTH];
	PathUtils::getBasename(basename, lengthOf(basename), shd_full_path);
	StaticString<MAX_PATH_LENGTH> bin_base_path(m_compiled_dir, "/", basename, "_");

	// For each pass, for each combinations of the defines, find the corresponding 
	// binary files (_vs.shb and _fs.shb), if they do not exist or are older than the
	// sources returns true
	for (int i = 0; i < combinations.pass_count; ++i)
	{
		const char* pass_path =
			StaticString<MAX_PATH_LENGTH>(bin_base_path, combinations.passes[i]);
		for (int j = 0; j < 1 << lengthOf(combinations.defines); ++j)
		{
			if ((j & (~combinations.vs_local_mask[i])) == 0)
			{
				const char* vs_bin_info =
					StaticString<MAX_PATH_LENGTH>(pass_path, j, "_vs.shb");
				if (!PlatformInterface::fileExists(vs_bin_info) ||
					PlatformInterface::getLastModified(vs_bin_info) < shd_last_modified)
				{
					return true;
				}
			}
			if ((j & (~combinations.fs_local_mask[i])) == 0)
			{
				const char* fs_bin_info =
					StaticString<MAX_PATH_LENGTH>(pass_path, j, "_fs.shb");
				if (!PlatformInterface::fileExists(fs_bin_info) ||
					PlatformInterface::getLastModified(fs_bin_info) < shd_last_modified)
				{
					return true;
				}
			}
		}
	}
	return false;
}

/// Compile the shader 'shd_path' (relative to basepath), adds HLSL debug info if 'debug'=true
void ShaderCompilerSolo::compile(const char* shd_path, bool debug)
{
	ShaderCombinations combinations;
	if (getCombinations(shd_path, &combinations))
		compile(shd_path, debug, combinations);
}

void ShaderCompilerSolo::compile(const char* shd_path, bool debug,
	const ShaderCombinations& combinations)
{
	// Avoid '_' in the base name as '_' is used to separate the shader name from the pass name
	char basename[MAX_PATH_LENGTH];
	PathUtils::getBasename(basename, lengthOf(basename), shd_path);
	if (findSubstring(basename, "_"))
	{
		g_log_error.log("ShaderCompiler") << "Shaders with underscore are not supported. " << shd_path
			<< " will not be compiled.";
		return;
	}

	for (int i = 0; i < combinations.pass_count; ++i)
	{
		compilePass(shd_path, true, combinations.passes[i], combinations.vs_local_mask[i], combinations.defines, debug);
		compilePass(shd_path, false, combinations.passes[i], combinations.fs_local_mask[i], combinations.defines, debug);
	}
}

/// Errors callback of the bgfx compiler
static void errorCallback(void*, const char* format, va_list args)
{
	char tmp[4096];
	vsnprintf(tmp, lengthOf(tmp), format, args);
	g_log_error.log("ShaderCompiler") << tmp;
}

/// Compile any combinations of the defines set in 'define_mask' of the pass 'pass'
/// of the shader 'shd_path'. The path must be relative to basepath. The indices
/// used in the define mask are local to the shader, 'all_defines' is the map
/// map[local_index] = renderer_index, by using the map we get the defines' names .
void ShaderCompilerSolo::compilePass(const char* shd_path,
	bool is_vertex_shader,
	const char* pass,
	int define_mask,
	const ShaderCombinations::Defines& all_defines,
	bool debug)
{
	// Each combination of the mask 'define_mask'
	for (int mask = 0; mask < 1 << lengthOf(all_defines); ++mask)
	{
		if ((mask & (~define_mask)) == 0)
		{
			// source: <path>/file.shd -> path/file_vs.sc
			StaticString<MAX_PATH_LENGTH> source_path(m_base_dir, shd_path);
			PathUtils::FileInfo shd_file_info(source_path);
			source_path = "";
			source_path << shd_file_info.m_dir << shd_file_info.m_basename << (is_vertex_shader ? "_vs.sc" : "_fs.sc");

			// out: <data_path>/pipelines/compiled/<shadername>_<pass><defines>_vs.shb
			StaticString<MAX_PATH_LENGTH> out_path(m_compiled_dir);
			out_path << shd_file_info.m_basename << "_" << pass;
			out_path << mask << (is_vertex_shader ? "_vs.shb" : "_fs.shb");

			const char* args_array[19];
			args_array[0] = "-f";
			args_array[1] = source_path;
			args_array[2] = "-o";
			args_array[3] = out_path;
			args_array[4] = "--depends";
			args_array[5] = "-i";
			StaticString<MAX_PATH_LENGTH> include(m_pipeline_dir);
			args_array[6] = include;
			args_array[7] = "--varyingdef";
			StaticString<MAX_PATH_LENGTH> varying(m_pipeline_dir, "varying.def.sc");
			args_array[8] = varying;
			args_array[9] = "--platform";
			if (m_is_opengl)
			{
				args_array[10] = "linux";
				args_array[11] = "--profile";
				args_array[12] = "140";
			}
			else
			{
				args_array[10] = "windows";
				args_array[11] = "--profile";
				args_array[12] = is_vertex_shader ? "vs_4_0" : "ps_4_0";
			}
			args_array[13] = "--type";
			args_array[14] = is_vertex_shader ? "vertex" : "fragment";
			if (debug)
			{
				args_array[15] = "--debug";
				args_array[16] = "--disasm";
			}
			else
			{
				args_array[15] = "-O";
				args_array[16] = "3";
			}
			// Load all the defines of the current mask
			args_array[17] = "--define";
			StaticString<256> defines(pass, ";");
			for (int i = 0; i < lengthOf(all_defines); ++i)
			{
				if (mask & (1 << i))
				{
					// From the local index 'i' get the index of the define in the renderer,
					// then get its string
					defines << getRenderer().getShaderDefine(all_defines[i]) << ";";
				}
			}
			if (stringLength(defines) >= lengthOf(defines.data)-1)
			{
				g_log_error.log("ShaderCompiler") << "Too many and long defines for pass " << pass 
					<< " of shader " << shd_path;
				continue;
			}
			args_array[18] = defines;
			// Set the error function for bgfx::compileShader
			bgfx::setShaderCErrorFunction(errorCallback, nullptr);

			// Note: the source of this function is:
			// ...\LumixEngine_3rdparty\3rdparty\bgfx\tools\shaderc\shaderc.cpp
			// which is included as cpp in:
			// ...\LumixEngine_3rdparty\misc\shaderc\error.cpp

			if (bgfx::compileShader(19, args_array) == EXIT_FAILURE)
			{
				g_log_error.log("ShaderCompiler") << "Failed to compile " << source_path 
					<< "(" << out_path << "), defines = \"" << defines << "\"";
			}
		}
	}
}


} // namespace