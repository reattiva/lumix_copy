
#include "shader_batch_compiler.h"
#include "engine/job_system.h"
#include "engine/path_utils.h"
#include "engine/log.h"
#include "engine/mt/thread.h"
#include "engine/profiler.h"
#include "editor/file_system_watcher.h"
#include "editor/platform_interface.h"
#include "engine/resource_manager.h"
#include "engine/resource_manager_base.h"

namespace Lumix
{

ShaderBatchCompiler::ShaderBatchCompiler(Engine& engine, 
										 const char* base_path,
										 IAllocator& allocator)
	: ShaderCompilerSolo(engine, base_path, allocator)
	, m_job_runnig(0)
	, m_job_empty_queue(1)
	, m_job_exit_request(false)
	, m_job_mutex(false)
	, m_job_compile_queue(m_allocator)
	, m_load_hook(*m_engine.getResourceManager().get(Shader::TYPE), *this)
	, m_changed_files(m_allocator)
	, m_dependencies(m_allocator)
	, m_shd_files(m_allocator)
	, m_hooked_files(m_allocator)
	, m_to_reload(m_allocator)
	, m_debug(false)
	, m_watcher(nullptr)
{
	// Create the compiler job
	JobSystem::JobDecl job;
	job.task = [](void* data) { ((ShaderBatchCompiler*)data)->compileTask(); };
	job.data = this;
	JobSystem::runJobs(&job, 1, nullptr);

	// Create a watcher for the files in the '<lumix data>\pipeline' folder
	m_watcher = FileSystemWatcher::create("pipelines", m_allocator);
	m_watcher->getCallback().bind<ShaderBatchCompiler, &ShaderBatchCompiler::onFileChanged>(this);

	// Search for all the shd files
	findShaderFiles("pipelines/");
	// Parse the d files and map a source file to the shaders depending on it
	parseDependencies();

	ResourceManagerBase* shader_manager = m_engine.getResourceManager().get(ShaderBinary::TYPE);
	shader_manager->setLoadHook(m_load_hook);
}

ShaderBatchCompiler::~ShaderBatchCompiler()
{
	// Stop the job and wait for it to exit
	m_job_exit_request = true;
	m_job_empty_queue = 0;
	JobSystem::wait(&m_job_runnig);

	FileSystemWatcher::destroy(m_watcher);

	//!! How to unhook?
}

/// Add a shd file to the queue and start the compile task.
/// The path must be relative to the data folder (e.g. pipeline\file.shd)
void ShaderBatchCompiler::queueFile(const char* shd_path)
{
	MT::SpinLock lock(m_job_mutex);
	m_job_compile_queue.emplace(shd_path);
	m_job_compile_queue.removeDuplicates();
	m_job_empty_queue = 0;
}

/// Task to compile all the shd files in the queue.
void ShaderBatchCompiler::compileTask()
{
	m_job_runnig = 1;
	for (;;)
	{
		// Wait while the queue is empty
		JobSystem::wait(&m_job_empty_queue);
		if (m_job_exit_request) break;

		if (m_callback.isValid())
			m_callback.invoke(true);

		// Take a shd file to compile from the queue
		{
			MT::SpinLock lock(m_job_mutex);
			m_job_current_file = m_job_compile_queue.back();
			m_job_compile_queue.pop();
			m_job_empty_queue = m_job_compile_queue.empty();
		}

		// Compile the file
		compile(m_job_current_file, m_debug);

		// The compiled file must be reloaded if it is in use
		{
			MT::SpinLock lock(m_job_mutex);
			m_to_reload.emplace(m_job_current_file, m_allocator);
			m_job_current_file = "";
		}

		if (m_callback.isValid() && m_job_empty_queue)
			m_callback.invoke(false);
	}
	m_job_runnig = 0;
}

//!! Why it is called only in the constructor? And if we create a new shd file?
/// Search for shd files in the folder 'src_dir' and its subfolders, store them
/// in 'm_shd_files'.
void ShaderBatchCompiler::findShaderFiles(const char* src_dir)
{
	if (!src_dir || stringLength(src_dir) == 0)
		return;

	auto* iter = PlatformInterface::createFileIterator(src_dir, m_allocator);
	PlatformInterface::FileInfo info;

	while (PlatformInterface::getNextFile(iter, &info))
	{
		// Recursive search in subfolders
		if (info.is_directory && info.filename[0] != '.')
		{
			StaticString<MAX_PATH_LENGTH> child(src_dir, "/", info.filename);
			findShaderFiles(child);
		}

		if (!PathUtils::hasExtension(info.filename, "shd")) continue;

		StaticString<MAX_PATH_LENGTH> shd_path(src_dir, "/", info.filename);
		PathUtils::normalize(shd_path.data);
		m_shd_files.emplace(shd_path, m_allocator);
	}

	PlatformInterface::destroyFileIterator(iter);
}

/// From a shader binary filename "(shadername)_(pass)(defines)_vs|fs.shb" or a shader source
/// filename "(shadername)_(pass)(defines)_vs|fs.sc" get the corresponding shd filename.
bool ShaderBatchCompiler::getSourceFromBinaryBasename(char* out, int out_max_size, const char* binary_basename)
{
	// Copy the binary basename up to the first underscore to get the shd basename
	char shd_basename[MAX_PATH_LENGTH];
	char* cout = shd_basename;
	const char* cin = binary_basename;
	while (*cin && *cin != '_')
	{
		*cout = *cin;
		++cout;
		++cin;
	}
	*cout = '\0';

	// Using the basename, search the full shd filename
	for (string& shd_path : m_shd_files)
	{
		char tmp[MAX_PATH_LENGTH];
		PathUtils::getBasename(tmp, lengthOf(tmp), shd_path.c_str());
		if (equalStrings(tmp, shd_basename))
		{
			copyString(out, out_max_size, shd_path.c_str());
			return true;
		}
	}

	g_log_info.log("ShaderCompiler") << binary_basename << " binary shader has no source code";
	return false;
}

static bool readLine(FS::IFile* file, char* out, int max_size)
{
	ASSERT(max_size > 0);
	char* c = out;

	while (c < out + max_size - 1)
	{
		if (!file->read(c, 1))
		{
			return (c != out);
		}
		if (*c == '\n') break;
		++c;
	}
	*c = '\0';
	return true;
}

/// Adds a depedency in the map 'm_dependencies': 'cvalue' (a shb file) depends on 'ckey' 
/// (a sh, sc or shd file). When 'ckey' changes we need to re-compiles all its 'cvalue'.
void ShaderBatchCompiler::addDependency(const char* ckey, const char* cvalue)
{
	char tmp[MAX_PATH_LENGTH];
	PathUtils::normalize(ckey, tmp, lengthOf(tmp));
	string key(tmp, m_allocator);

	int idx = m_dependencies.find(key);
	if (idx < 0)
	{
		idx = m_dependencies.insert(key, Array<string>(m_allocator));
	}
	m_dependencies.at(idx).emplace(cvalue, m_allocator);
}

/// Build the depedency map by reading the d files in the compiled folder, they contain
/// the filename of the binary shader and its source files.
void ShaderBatchCompiler::parseDependencies()
{
	m_dependencies.clear();

	auto* iter = PlatformInterface::createFileIterator(getCompiledPath(), m_allocator);

	auto& fs = m_engine.getFileSystem();
	PlatformInterface::FileInfo info;
	while (PlatformInterface::getNextFile(iter, &info))
	{
		if (!PathUtils::hasExtension(info.filename, "d")) continue;

		auto* file = fs.open(fs.getDiskDevice(),
			Path(StaticString<MAX_PATH_LENGTH>(getCompiledPath(), "/", info.filename)),
			FS::Mode::READ | FS::Mode::OPEN);
		if (!file)
		{
			g_log_error.log("ShaderCompiler") << "Could not open: " << info.filename;
			continue;
		}

		// Read the first file and trim it, it is the shader binary filename (shb)
		char first_line[100];
		readLine(file, first_line, sizeof(first_line));
		for (int i = 0; i < sizeof(first_line); ++i)
		{
			if (first_line[i] == '\0' || first_line[i] == ' ')
			{
				first_line[i] = '\0';
				break;
			}
		}

		// Read the other lines, they are the shaders sources (sc, sh) on which the
		// binary depends
		char line[100];
		while (readLine(file, line, sizeof(line)))
		{
			char* trimmed_line = trimmed(line);
			char* c = trimmed_line;
			while (*c)
			{
				if (*c == ' ') break;
				++c;
			}
			*c = '\0';

			// The binary shader (shb) depends on its sources (sc, sh)
			addDependency(trimmed_line, first_line);
		}

		// Get the shd file from the shader binary filename
		char basename[MAX_PATH_LENGTH];
		char src[MAX_PATH_LENGTH];
		PathUtils::getBasename(basename, sizeof(basename), first_line);
		if (getSourceFromBinaryBasename(src, sizeof(src), basename))
		{
			// The binary shader (shb) depends on its shd file
			addDependency(src, first_line);
		}

		fs.close(*file);
	}

	PlatformInterface::destroyFileIterator(iter);
}

/// On a change of a file in the pipeline folder and subfolders, if it is a shader source 
/// file (extension shd, sc, sh) add it to 'm_changed_files' to be processed
void ShaderBatchCompiler::onFileChanged(const char* path)
{
	char ext[10];
	PathUtils::getExtension(ext, sizeof(ext), path);
	if (!equalStrings("sc", ext) && !equalStrings("shd", ext) && !equalStrings("sh", ext)) return;

	char normalized[MAX_PATH_LENGTH];
	copyString(normalized, "pipelines/");
	catString(normalized, path);
	PathUtils::normalize(normalized);

	{
		MT::SpinLock lock(m_job_mutex);
		m_changed_files.push(string(normalized, m_allocator));
	}
}

/// Process the changed source files: find the shaders which are affected, find the 
/// corresponding shd files, add them to the compiler queue
void ShaderBatchCompiler::processChangedFiles()
{
	if (!m_job_empty_queue) return;

	char changed_file_path[MAX_PATH_LENGTH];

	// Take a changed file (shd, sc, sh) from the list
	{
		MT::SpinLock lock(m_job_mutex);
		if (m_changed_files.empty()) return;

		m_changed_files.removeDuplicates();
		const char* tmp = m_changed_files.back().c_str();
		copyString(changed_file_path, sizeof(changed_file_path), tmp);
		m_changed_files.pop();
	}

	// Search for this file into the dependencies map, this map stores for each source
	// file (shd, sc, sh) all the binary shaders (shb) which depend on it.
	string tmp_string(changed_file_path, m_allocator);
	int find_idx = m_dependencies.find(tmp_string);

	// If not found and it is a shader code (sc), search for the corresponding 
	// shd file, then with it search in the dependencies again
	if (find_idx < 0)
	{
		int len = stringLength(changed_file_path);
		if (len <= 6) return;
		if (equalStrings(changed_file_path + len - 6, "_fs.sc") ||
			equalStrings(changed_file_path + len - 6, "_vs.sc"))
		{
			copyString(
				changed_file_path + len - 6, lengthOf(changed_file_path) - len + 6, ".shd");
			tmp_string = changed_file_path;

			find_idx = m_dependencies.find(tmp_string);
		}
	}

	// If the file is a dependency
	if (find_idx >= 0)
	{
		if (PathUtils::hasExtension(changed_file_path, "shd"))
		{
			// Add the shd file to the compiler queue
			queueFile(changed_file_path);
		}
		else
		{
			Array<StaticString<MAX_PATH_LENGTH>> src_list(m_allocator);

			// For each binary shader which has this dependency, get the corresponding shd file
			for (auto& bin : m_dependencies.at(find_idx))
			{
				char basename[MAX_PATH_LENGTH];
				PathUtils::getBasename(basename, sizeof(basename), bin.c_str());
				char tmp[MAX_PATH_LENGTH];
				if (getSourceFromBinaryBasename(tmp, sizeof(tmp), basename))
				{
					src_list.emplace(tmp);
				}
			}

			src_list.removeDuplicates();

			// Add the shd files to the compiler queue
			for (auto& src : src_list)
			{
				queueFile(src);
			}
		}
	}
}

/// Wait for the compiler to compile all the files
void ShaderBatchCompiler::wait()
{
	while (!m_job_empty_queue)
	{
		MT::sleep(5);
	}
}

/// Add to the queue all the shd files which needs to be compiled because they are new 
/// or because the sources changed. If 'wait' is true, wait for the compiler to finish.
/// This function is exported to Lua.
void ShaderBatchCompiler::makeUpToDate(bool wait)
{
	// Exit if the compiler is working
	if (!m_job_empty_queue)
	{
		if (wait) this->wait();
		return;
	}
	if (m_shd_files.empty()) return;

	// For each shd files, add it to the queue if it or its sources are more recent than
	// the corresponding binary files
	for (string& shd_path : m_shd_files)
	{
		ShaderCombinations combinations;
		if (!getCombinations(shd_path.c_str(), &combinations))
		{
			g_log_error.log("ShaderCompiler") << "Could not open: " << shd_path;
			continue;
		}

		if (isChanged(shd_path.c_str(), combinations))
		{
			queueFile(shd_path.c_str());
		}
	}

	// Search in dependencies for binary shader which are older than its source files,
	// add to the queue the shd file corresponding to the binary shader 
	for (int i = 0; i < m_dependencies.size(); ++i)
	{
		// Source file (shd, sc, sh)
		auto& key = m_dependencies.getKey(i);
		// Array of binary shader (shb) which depend on the source file
		auto& value = m_dependencies.at(i);
		for (auto& bin : value)
		{
			if (!PlatformInterface::fileExists(bin.c_str()) ||
				PlatformInterface::getLastModified(bin.c_str()) <
				PlatformInterface::getLastModified(key.c_str()))
			{
				char basename[MAX_PATH_LENGTH];
				PathUtils::getBasename(basename, sizeof(basename), bin.c_str());
				char tmp[MAX_PATH_LENGTH];
				if (getSourceFromBinaryBasename(tmp, sizeof(tmp), basename))
				{
					queueFile(tmp);
				}
			}
		}
	}

	if (wait) this->wait();
}

ShaderBatchCompiler::ShaderLoadHook::ShaderLoadHook(ResourceManagerBase& manager, ShaderBatchCompiler& compiler)
	: ResourceManagerBase::LoadHook(manager)
	, m_compiler(compiler)
{
}

/// Before loading a non existing binary shader (shb), queue its shd file to be compiled, 
/// add it to 'm_hooked_files'
bool ShaderBatchCompiler::ShaderLoadHook::onBeforeLoad(Resource& resource)
{
	if (PlatformInterface::fileExists(resource.getPath().c_str())) return false;

	// Get the shd filename, if it is not in the compiler queue, nor in the reload list (already compiled
	// files), nor it is current compiling, then add it to the queue to be compiled
	char source_path[MAX_PATH_LENGTH];
	PathUtils::getBasename(source_path, lengthOf(source_path), resource.getPath().c_str());
	if (m_compiler.getSourceFromBinaryBasename(source_path, lengthOf(source_path), source_path))
	{
		MT::SpinLock lock(m_compiler.m_job_mutex);
		if (m_compiler.m_job_current_file != source_path
			&& m_compiler.m_to_reload.find([&source_path](const string& str) { return str == source_path; }) < 0
			&& m_compiler.m_job_compile_queue.find([&source_path](const auto& str) { return str == source_path; }) < 0)
		{
			m_compiler.m_job_compile_queue.emplace(source_path);
			m_compiler.m_job_empty_queue = 0;
		}
		// We need to keep track of the files whose loading we've interrupted
		m_compiler.m_hooked_files.push(&resource);
		m_compiler.m_hooked_files.removeDuplicates();
	}

	// We take charge of loading the resource later by calling continueLoad
	return true;
}

/// Reload or continue the loading of each compiled shader 
void ShaderBatchCompiler::reloadShaders()
{
	m_to_reload.removeDuplicates();

	auto shader_manager = m_engine.getResourceManager().get(Shader::TYPE);
	for (string& shd_path : m_to_reload)
	{
		// If we have interrupted the loading of this shader, let it continue here
		bool any_hooked = false;
		for (int i = m_hooked_files.size() - 1; i >= 0; --i)
		{
			Resource* res = m_hooked_files[i];
			const char* shader_path = ((ShaderBinary*)res)->m_shader->getPath().c_str();
			if (equalStrings(shader_path, shd_path.c_str()))
			{
				any_hooked = true;
				m_load_hook.continueLoad(*res);
				m_hooked_files.eraseFast(i);
			}
		}

		// Otherwise reload the shader (it does nothing if the shader was not loaded)
		if (!any_hooked) shader_manager->reload(Path(shd_path.c_str()));
	}

	m_to_reload.clear();
}

// void StudioAppImpl::update() -> plugin->update(time_delta); -> struct ShaderEditorPlugin::update(float)
void ShaderBatchCompiler::update()
{
	PROFILE_FUNCTION();

	// Find the shaders whose sources changed, queue the corresponding shd files
	processChangedFiles();

	// If we've compiled some shaders
	MT::SpinLock lock(m_job_mutex);
	if (!m_to_reload.empty())
	{
		// Reload or continue the loading of each compiled shader 
		reloadShaders();
		// Discover which source files each shader depends on, this is here
		// because it is the compiler which creates the dependencies (d) files
		parseDependencies();
	}
}

}