#pragma once

#include "renderer/shader.h"

namespace Lumix
{

class Engine;
struct IAllocator;
class Renderer;

class ShaderCompilerSolo
{
public:
	enum Flags : u32
	{
		ERROR_NO_PIPELINE_FOLDER = 1 << 0,
		ERROR_NO_OUTPUT_FOLDER = 1 << 1,
	};

public:
	ShaderCompilerSolo(Engine& engine, const char* base_path, IAllocator& allocator);

	const char* getBasePath() const { return m_base_dir; }
	bool setBasePath(const char* base_path);
	const char* getPipelinePath() const { return m_pipeline_dir; }
	const char* getCompiledPath() const { return m_compiled_dir; }
	u32 getFlags() const { return m_flags; }

	void compile(const char* path, bool debug);
	bool isChanged(const char* shd_path) const;
	bool compileIfChanged(const char* path, bool debug);

protected:
	Renderer& getRenderer() const;
	bool getCombinations(const char* shd_fs_path, 
		ShaderCombinations* output) const;
	bool isChanged(const char* shd_path,
		const ShaderCombinations& combinations) const;
	void compile(const char* shd_fs_path, bool debug,
		const ShaderCombinations& combinations);
	void compilePass(const char* path,
		bool is_vertex_shader,
		const char* pass,
		int define_mask,
		const ShaderCombinations::Defines& all_defines,
		bool debug);

protected:
	Engine& m_engine;
	IAllocator& m_allocator;
	u32 m_flags;

private:
	bool m_is_opengl;
	StaticString<MAX_PATH_LENGTH> m_base_dir;
	StaticString<MAX_PATH_LENGTH> m_pipeline_dir;
	StaticString<MAX_PATH_LENGTH> m_compiled_dir;
};

}